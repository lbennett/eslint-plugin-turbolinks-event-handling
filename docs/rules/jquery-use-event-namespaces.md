# `.on('event')` should be namespaced `.on('event.namespace')` (jquery-use-event-namespaces)

When registering event listeners, use event namespaces to have finer control over event listeners.

## Rule Details

Examples of **incorrect** code for this rule:

```js
$element.on('click', clickListener);
```

```js
$element.off('scroll');
```

Examples of **correct** code for this rule:

```js
$element.on('click.namespace', clickListener);
```

```js
$element.off('scroll.namespace');
```
