# `.addEventListener(eventName) should be passed a referenced listener function` (vanilla-addeventlistener-needs-function-reference)

When calling `.addEventListener()`, the second argument should be a referenced listener function.

## Rule Details

Examples of **incorrect** code for this rule:

```js
element.addEventListener('scroll', () => { this.clickListener(); });
```

```js
element.addEventListener('change', function () { this.clickListener(); });
```

```js
element.addEventListener('click', clickListener.bind(this));
```

Examples of **correct** code for this rule:

```js
element.addEventListener('click', clickListener);
```

```js
element.addEventListener('click', clickListener);
```
