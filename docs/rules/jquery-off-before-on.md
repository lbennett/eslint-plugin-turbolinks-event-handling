# `.off(eventName)` should be called before `.on(eventName)` (jquery-off-before-on)

`.off` should always be called in the same chain as `.on` to ensure no event listeners are being registered twice.

## Rule Details

Examples of **incorrect** code for this rule:

```js
$element.on('click', clickListener);
```

```js
$element.off('scroll'); $element.on('scroll', scrollListener);
```

```js
const someObjectLiteral = {
  someFunction(element) {
    this.element = element;
    this.bindOff();
    this.bindOn();
  }
  bindOff() {
    this.element.off('change');
  }
  bindOn() {
    this.element.on('change', changeListener);
  }
};
```

Examples of **correct** code for this rule:

```js
$element.off('click').on('click', clickListener);
```
