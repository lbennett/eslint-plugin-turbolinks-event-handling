# @lbennett/eslint-plugin-turbolinks-event-handling

ESLint plugin to avoid registering vanilla or jquery event listeners multiple times when using turbolinks

## Installation

```
$ npm install @lbennett/eslint-plugin-turbolinks-event-handling --save-dev
```

```
$ yarn add @lbennett/eslint-plugin-turbolinks-event-handling --dev
```

## Usage

Add `@lbennett/turbolinks-event-handling` to the plugins section of your `.eslintrc` configuration file.

```json
{
  "plugins": [
    "@lbennett/turbolinks-event-handling"
  ]
}
```

Then configure the rules you want to use under the rules section.

```json
{
  "rules": {
    "@lbennett/turbolinks-event-handling/jquery-off-before-on": 2,
    "@lbennett/turbolinks-event-handling/jquery-use-event-namespaces": 2,
    "@lbennett/turbolinks-event-handling/vanilla-addeventlistener-needs-function-reference": 2
  }
}
```
