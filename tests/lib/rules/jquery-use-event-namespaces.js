/**
 * @fileoverview `.on('event')` should be namespaced `.on('event.namespace')`
 * @author Luke Bennett
 */

const rule = require('../../../lib/rules/jquery-use-event-namespaces');
const RuleTester = require('eslint').RuleTester;

const ruleTester = new RuleTester();

ruleTester.run('jquery-use-event-namespaces', rule, {
  valid: [
    "$element.on('click.namespace', clickListener);",
    "$element.off('scroll.namespace');",
  ],
  invalid: [
    {
      code: "$element.on('click', clickListener);",
      errors: [{ message: "`.on('click')` event should be namespaced `.on('click.namespace')`" }],
    },
    {
      code: "$element.off('scroll');",
      errors: [{ message: "`.off('scroll')` event should be namespaced `.off('scroll.namespace')`" }],
    },
  ],
});
