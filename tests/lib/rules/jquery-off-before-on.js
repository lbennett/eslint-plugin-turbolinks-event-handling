/**
 * @fileoverview `.off(eventName)` should be called before `.on(eventName)`
 * @author Luke Bennett
 */

const rule = require('../../../lib/rules/jquery-off-before-on');
const RuleTester = require('eslint').RuleTester;

const ruleTester = new RuleTester();

ruleTester.run('jquery-off-before-on', rule, {
  valid: [
    "$element.off('click').on('click', clickListener);",
  ],
  invalid: [
    {
      code: "$element.on('click', clickListener);",
      errors: [{ message: "`.off('click')` should be called in a chain before `.on('click')`" }],
    },
    {
      code: "$element.off('scroll').on('click', clickListener);",
      errors: [{ message: "`.off('click')` should be called in a chain before `.on('click')`" }],
    },
    {
      code: "$element.off('scroll'); $element.on('scroll', scrollListener);",
      errors: [{ message: "`.off('scroll')` should be called in a chain before `.on('scroll')`" }],
    },
    {
      code: `
        var someObjectLiteral = {
          someFunction: function(element) {
            this.element = element;
            this.bindOff();
            this.bindOn();
          },
          bindOff: function() {
            this.element.off('change');
          },
          bindOn: function() {
            this.element.on('change', changeListener);
          },
        };
      `,
      errors: [{ message: "`.off('change')` should be called in a chain before `.on('change')`" }],
    },
  ],
});
