/**
 * @fileoverview `.addEventListener(eventName) should be passed a referenced listener function`
 * @author Luke Bennett
 */

const rule = require('../../../lib/rules/vanilla-addeventlistener-needs-function-reference');
const RuleTester = require('eslint').RuleTester;

const ruleTester = new RuleTester();

ruleTester.run('vanilla-addeventlistener-needs-function-reference', rule, {
  valid: [
    "element.addEventListener('click', clickListener);",
    "element.addEventListener('click', this.clickListener);",
  ],
  invalid: [
    {
      code: "element.addEventListener('scroll', () => { this.clickListener(); });",
      errors: [{ message: "`.addEventListener('scroll')` listener should be a referenced function." }],
    },
    {
      code: "element.addEventListener('change', function () { this.clickListener(); });",
      errors: [{ message: "`.addEventListener('change')` listener should be a referenced function." }],
    },
    {
      code: "element.addEventListener('click', clickListener.bind(this));",
      errors: [{ message: "`.addEventListener('click')` listener should be a referenced function." }],
    },
  ],
});
