/**
 * @fileoverview `.addEventListener(eventName) should be passed a referenced listener function`
 * @author Luke Bennett
 */

module.exports = {
  meta: {
    docs: {
      description: '`.addEventListener(eventName) should be passed a referenced listener function`',
      category: 'Best Practices',
      recommended: true,
    },
    fixable: null,
    schema: [],
  },
  create(context) {
    // console.log(context);
    return {};
  },
};
