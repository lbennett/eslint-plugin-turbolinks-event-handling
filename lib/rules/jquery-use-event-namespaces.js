/**
 * @fileoverview `.on('event')` should be namespaced `.on('event.namespace')`
 * @author Luke Bennett
 */

module.exports = {
  meta: {
    docs: {
      description: "`.on('event')` should be namespaced `.on('event.namespace')`",
      category: 'Best Practices',
      recommended: true,
    },
    fixable: null,
    schema: [],
  },
  create(context) {
    return {
      CallExpression: function CallExpression(node) {
        if (node.type !== 'CallExpression') return;
        if (node.callee.type !== 'MemberExpression') return;

        if (!/^(on|off)$/.test(node.callee.property.name)) return;

        if (node.arguments.length === 0) return;
        if (node.arguments[0].type !== 'Literal') return;

        const eventValue = node.arguments[0].value.toString() || '';

        if (/^.+\..+$/.test(eventValue)) return;

        context.report({
          node,
          message: `\`.${node.callee.property.name}('${eventValue}')\` event should\
 be namespaced \`.${node.callee.property.name}('${eventValue}.namespace')\``,
        });
      },
    };
  },
};
