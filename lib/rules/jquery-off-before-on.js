/**
 * @fileoverview `.off(eventName)` should be called before `.on(eventName)`
 * @author Luke Bennett
 */

module.exports = {
  meta: {
    docs: {
      description: '`.off(eventName)` should be called in a chain before `.on(eventName)`',
      category: 'Best Practices',
      recommended: true,
    },
    fixable: null,
    schema: [],
  },
  create(context) {
    return {
      CallExpression: function CallExpression(node) {
        if (node.type !== 'CallExpression') return;
        if (node.callee.type !== 'MemberExpression') return;
        if (node.callee.object.type !== 'CallExpression') return;
        if (node.callee.object.callee.type !== 'MemberExpression') return;

        if (node.callee.property.name !== 'on') return;

        if (node.arguments.length === 0) return;
        if (node.callee.object.arguments.length === 0) return;
        if (node.arguments[0].type !== 'Literal') return;
        if (node.callee.object.arguments[0].type !== 'Literal') return;

        const onValue = node.arguments[0].value.toString() || '';
        const offValue = node.callee.object.arguments[0].value.toString() || '';

        if (node.callee.object.callee.property.name === 'off' && onValue === offValue) return;

        context.report({
          node,
          message: `\`.off('${onValue}')\` should be called in a chain before \`.on('${onValue}')\``,
        });
      },
    };
  },
};
