/**
 * @fileoverview ESLint plugin to avoid registering vanilla or jquery event
 *               listeners multiple times when using turbolinks
 * @author Luke Bennett
 */


const path = require('path');
const requireIndex = require('requireindex');

module.exports.rules = requireIndex(path.join(__dirname, '/rules'));
